# Laravel base Docker

* This a base docker laravel container , I remain open to suggestions and contributions

## Getting Started

Create a docker-container to learn laravel

### Prerequisities

```
git
docker
https://docs.docker.com/engine/installation/
docker-compose
https://docs.docker.com/compose/install/
```

### Installing

```
$ git clone git@bitbucket.org:ABARRANCOJOBOMAS/laravel-base.git laravel-base
$ cd laravel-base
$ docker-compose up --build
```
 In another terminal
```
$ docker exec -it abarranco-laravel  bash  -c "/bin/bash /tmp/init.sh" 
```
 add to /etc/hosts  
	127.0.0.1 dev.laravel-base.com
```
$ sudo chown -R <YOUR-USER>.root code/laravel-base 
$ sudo chmod -R 0777 code/laravel-base/storage

```
go to dev.laravel-base.com:8080 on your browser 

in laravel-base/code/laravel-base  modify the code in real time 

ENJOY!!

## Built With

* bash
* vim
